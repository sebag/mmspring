package com.sg.mmservicehello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MmServiceHelloApplication {

	public static void main(String[] args) {
		SpringApplication.run(MmServiceHelloApplication.class, args);
	}
}
